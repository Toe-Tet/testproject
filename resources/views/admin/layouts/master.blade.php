<!DOCTYPE html>
<html lang="en">

@yield('title')

@include('admin.layouts.head')

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->

@include('admin.layouts.nav')

  <div class="content-wrapper">
    <div class="container-fluid">
    
    @yield('content')

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>

    @include('admin.layouts.scripts')
    
    @stack('script')
  </div>
</body>

</html>
