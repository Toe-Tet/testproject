<a href="{{ route('itemtypes.edit', $itemtype->id) }}" class="btn btn-primary"> Edit </a>

<form action="{{ route('itemtypes.destroy', $itemtype->id) }}" method="POST" style="display: inline;" id="delete-form">
	{{ csrf_field() }}
	{{ method_field('DELETE') }}

	<button type="submit" class="btn btn-danger" id="delete-btn"> Delete </button>
</form>