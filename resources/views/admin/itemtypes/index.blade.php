
@extends('admin.layouts.master')

@section('title')

	<title>Item Types</title>

@endsection

@section('content')
<div class="row">
    <div class="col-md-10">
    </div>
    <div class="col-md-2">
        <div class="container">
            <a href="{{ route('itemtypes.create') }}" class="btn btn-primary text-right">
                Register
            </a>
        </div>  
    </div>
</div>

<hr>
<!-- /.row -->
<div class="container">
    <h3>Item Types List</h3>
    <table class="table table-bordered" id="categoryitemtypes-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Item Type Name</th>
                <th>Keyword</th>
                <th>Category</th>
{{--                 <th>Category Item Type</th> --}}
                <th>Datetime</th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>
@endsection

@push('script')
<script>
$(function() {
    $('#categoryitemtypes-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('itemtype.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'keyword', name: 'keyword' },
            { data: 'categoryid', name: 'categoryid' },
            // { data: 'categoryitemtypeid', name: 'categoryitemtypeid' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endpush