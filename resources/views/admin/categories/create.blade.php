
@extends('admin.layouts.master')

@section('title')

	<title>Categories Register</title>

@endsection

@section('content')

	<div class="container">

		<h3>Categories Registration</h3>
		<form action="{{ route('categories.store') }}" method="POST">
			@csrf

			<div class="container" style="margin-top: 20px;">
				<label for="name" class="form-label">
					Category Name
				</label>
				<input type="text" class="form-control" name="name" required>
			</div>

			<div class="container" style="margin-top: 20px;">
				<label for="keyword" class="form-label">
					Keyword
				</label>
				<input type="text" class="form-control" name="keyword" required>
			</div>

			<div class="container" style="margin-top: 20px;">
				<input type="submit" class="btn btn-primary" value="Register">
			</div>


			

		</form>
	</div>
@endsection