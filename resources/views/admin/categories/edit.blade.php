
@extends('admin.layouts.master')

@section('title')

	<title>Category Edit</title>

@endsection

@section('content')

	<div class="container">

		<h3>Category Edit</h3>
		@foreach($categories as $category)
			<form action="{{ route('categories.update', $category->id) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}

				<div class="container" style="margin-top: 20px;">
					<label for="name" class="form-label">
						Category Name
					</label>
					<input type="text" class="form-control" name="name" value="{{ old('name', $category->name) }}" required>
				</div>

				<div class="container" style="margin-top: 20px;">
					<label for="keyword" class="form-label">
						Keyword
					</label>
					<input type="text" class="form-control" name="keyword" value="{{ old('keyword', $category->keyword) }}" required>
				</div>

				<div class="container" style="margin-top: 20px;">
					<input type="submit" class="btn btn-primary" value="Update">
				</div>

			</form>
		@endforeach
	</div>
@endsection