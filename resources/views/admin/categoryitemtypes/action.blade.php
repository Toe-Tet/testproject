<a href="{{ route('categoryitemtypes.edit', $categoryitemtype->id) }}" class="btn btn-primary"> Edit </a>

<form action="{{ route('categoryitemtypes.destroy', $categoryitemtype->id) }}" method="POST" style="display: inline;" id="delete-form">
	{{ csrf_field() }}
	{{ method_field('DELETE') }}

	<button type="submit" class="btn btn-danger" id="delete-btn"> Delete </button>
</form>