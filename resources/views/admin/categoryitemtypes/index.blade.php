
@extends('admin.layouts.master')

@section('title')

	<title>Category Item Types</title>

@endsection

@section('content')
<div class="row">
    <div class="col-md-10">
    </div>
    <div class="col-md-2">
        <div class="container">
            <a href="{{ route('categoryitemtypes.create') }}" class="btn btn-primary text-right">
                Register
            </a>
        </div>  
    </div>
</div>

<hr>
<!-- /.row -->
<div class="container">
    <h3>Category Item Types List</h3>
    <table class="table table-bordered" id="categoryitemtypes-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Category Item Type Name</th>
                <th>Datetime</th>
                <th></th>
            </tr>
        </thead>
    </table>
</div>
@endsection

@push('script')
<script>
$(function() {
    $('#categoryitemtypes-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('categoryitemtype.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endpush