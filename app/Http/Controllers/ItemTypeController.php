<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItemType;
use DataTables;
use App\Category;
use App\CategoryItemType;

class ItemTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.itemtypes.index');
    }

    public function test()
    {
        dd(1);
    }

    public function itemtypeData(Request $request)
    {

        // $itemType = ItemType::all();
        // $categories = Category::all();
            // $itemType = Category::with()
        $itemType = ItemType::with('categories')->select('name')->first();
        dd($itemType->getCategories());
        dd($itemType);
        // $item = $categories->name;
        // dd($item);
        // dd($item);
        // $categories = Category::select('name')->get();
        return Datatables::of(ItemType::all())
        ->addColumn('action', function($itemtype){
            return view('admin.itemtypes.action', compact('itemtype'))->render();
        })
        ->editColumn('categoryid', function($itemtype) {
                    return $itemtype->categories->name;
                })
        ->make(true);


        // $categories = Category::get()->unique('categoryid')->values();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::select('id', 'name')->get();
        $categoryitemtypes = CategoryItemType::select('id', 'name')->get();
        return view('admin.itemtypes.create', compact('categories', 'categoryitemtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->input('withgender'));
        $this->validate($request, [
            'name' => 'required|string',
            'keyword' => 'required|string',
            'category' => 'required',
            'categoryitemtype' => 'required'
        ]);

        ItemType::create([
            'name' => $request->input('name'),
            'keyword' => $request->input('keyword'),
            'categoryid' => $request->input('category'),
            'categoryitemtypeid' => $request->input('categoryitemtype'),
        ]);
        
        return redirect()->route('itemtypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itemtypes = ItemType::where('id', '=', $id)->get();
        return view('admin.itemtypes.edit', compact('itemtypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'keyword' => 'required|string',
            'categoryid' => 'required',
            'categoryitemtypeid' => 'required'
        ]);

        $itemtype = ItemType::find($id);
        $itemtype->update($request->all());
        
        return redirect()->route('itemtypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $itemtype = ItemType::find($id);
        $itemtype->delete();

        return back();
    }
}
