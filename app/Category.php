<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'keyword'
    ];

    public function itemtypes()
    {
        return $this->hasMany('App\ItemType');
    }

    public function getName()
    {
    	return $this->name;
    }
}
